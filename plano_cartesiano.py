#Tarea 4 Unidad 2
#Ubicacion de las coordenadas de la tarea 3.

import pygame
import random
BLANCO = (255, 255, 255)
AMARILLO = (255, 255, 0)
NEGRO = (0, 0, 0)
pygame.init()
Dimensiones = (600, 600)
Pantalla = pygame.display.set_mode(Dimensiones)
pygame.display.set_caption("Plano Cartesiano")


Fuente = pygame.font.Font(None, 20)
Texto = Fuente.render("Ubicacion de las coordenadas", True, NEGRO)
X= Fuente.render("X", True, NEGRO)
Y = Fuente.render("Y", True, BLANCO)
Cuadrado =Fuente.render("250 X 120", True, NEGRO)
Uno = Fuente.render("1", True, NEGRO)
Dos = Fuente.render("2", True, NEGRO)
Uno1 = Fuente.render("1", True, NEGRO)
Dos2 = Fuente.render("2", True, NEGRO)
Var = Fuente.render("250", True, NEGRO)
Var2 = Fuente.render("120", True, NEGRO)


Terminar = False
reloj = pygame.time.Clock()

while not Terminar:
     for Evento in pygame.event.get():
        if Evento.type == pygame.QUIT:
            Terminar = True

     Pantalla.fill(BLANCO)
     pygame.draw.line(Pantalla, NEGRO, [595,300],[5,300],3)
     pygame.draw.line(Pantalla, NEGRO, [300, 5], [300, 595], 3)
     pygame.draw.rect(Pantalla, AMARILLO, (300, 300, 250, 120), 0)
     Pantalla.blit(Texto, [10, 10])
     Pantalla.blit(X, [585, 280])
     Pantalla.blit(Y, [310, 585])
     Pantalla.blit(Uno, [290, 310])
     Pantalla.blit(Dos, [290, 340])
     Pantalla.blit(Uno1, [310, 280])
     Pantalla.blit(Dos2, [330, 280])
     Pantalla.blit(Var, [540, 280])
     Pantalla.blit(Var2, [275, 410])
     Pantalla.blit(Cuadrado, [400, 350])



     pygame.display.flip()
     reloj.tick(20)
pygame.quit()




